﻿
CREATE TABLE produtos(
	id serial, --PRIMARY KEY 
	descricao varchar(70), --UNIQUE
	created date,
	CONSTRAINT produtosPk PRIMARY KEY (id),
	CONSTRAINT produtosUk UNIQUE (descricao)
);
CREATE TABLE colheitas(
	id serial,
	produto_id integer,
	quantidade numeric,
	dataColheita date,
	CONSTRAINT colheitasPk PRIMARY KEY (ID),
	CONSTRAINT produtosFk FOREIGN KEY (produto_id) REFERENCES produtos(id) MATCH FULL NOT DEFERRABLE
);

INSERT INTO produtos (descricao, created) values
('Pimentão', '17/11/2013'),
('Tomate', '17/11/2013'),
('Cebola', '17/11/2013'),
('Batata', '17/11/2013');

